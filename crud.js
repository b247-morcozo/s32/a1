const http = require('http')

const port = 4000

const server = http.createServer((request, response) => {

	if (request.url == "/updatecourse" && request.method == "GET"){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Update a course to our resources')

	} else if (request.url == "/archivecourse" && request.method == "GET"){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Archive courses to our resources')
	
	} 

})

// Uses the "server" and "port" variable created above.
server.listen(port);

// When server is running, console will the print the message:
console.log(`Server now accessible at localhost:${port}.`)