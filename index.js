const http = require('http')

const port = 4000

const server = http.createServer((request, response) => {

	if (request.url == "/" && request.method == "GET"){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to Booking System')

	} else if (request.url == "/profile" && request.method == "GET"){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to your profile!')
	
	} else if (request.url == "/courses" && request.method == "GET"){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Here’s our courses available')
	
	} else if (request.url == "/addcourse" && request.method == "GET"){

		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Add a course to our resources')
	
	} 

})

// Uses the "server" and "port" variable created above.
server.listen(port);

// When server is running, console will the print the message:
console.log(`Server now accessible at localhost:${port}.`)